Apple II Raspberry Pi Adapter (v2.0)
====================================

This is a redesign of a redesign of the Apple II Pi
(http://schmenk.is-a-geek.com/wordpress/?cat=10), with the following goals:

1. Keep the design in files with an open format, editable with
   freely-available design software.  (This time, it's in KiCad,
   which seems to be getting a bit more traction than gEDA.)
2. Replace through-hole components with surface-mount components to the
   extent that it's possible to do so.  (It should still be hand-
   solderable, though an inexpensive hot-air rework station of the 
   "858D" type would be better for SMD parts than a soldering iron.)
3. Enable future improvements to the design by anybody interested in doing
   so (item 1 helps considerably with this).

Discussion in
[comp.sys.apple2](https://groups.google.com/forum/#!topic/comp.sys.apple2/ogb335ww8-w)
has already led to one change: provision has been made to clock the 6551
with either a crystal (and, if needed, an extra capacitor) or the GPIO4 line
from the Raspberry Pi.  Also, I had originally planned on using the WDC
W65C51N in the LQFP-32 package, but there's a
[bug](https://en.wikipedia.org/wiki/WDC_65C51) in the W65C51N that indicate
it might be a better idea to use new-old-stock 6551s from other suppliers.

That is why the 6551 is still using the old DIP-28
footprint.

My [initial design](https://gitlab.com/salfter/A2RPi) worked, but wasn't
without issues.  Most importantly, at least with the Raspberry Pi Model B,
there was interference between the 6551 on the adapter and the electrolytic
capacitor next to the micro-USB jack on the Raspberry Pi.  This design
places nothing on the board under the Raspberry Pi that could interfere with
it.

Also, while it doesn't use any of the extra pins, it now has the 40-pin GPIO
connector used by current Raspberry Pi models.  Original models (with the
26-pin header) will still work if you build the board with a 26-pin header;
a 40-pin header will interfere with the composite video output.

This design depends on symbols and footprints I have modified and/or created.
They're included as a submodule from
[my library](https://gitlab.alfter.us/salfter/kicad-libs).

Pull them in with something like this: 

`git submodule init && git submodule update --recursive --remote`

You can find this project on
[Hackaday](https://hackaday.io/project/46430-a2rpi-v20).
